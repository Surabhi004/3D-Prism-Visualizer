#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <iostream>
#include <cmath>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>

#include "shader.h"

void framebuffer_size_callback(GLFWwindow *window, int width, int height);
void processInput(GLFWwindow *window);

// settings
const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 600;

const char *vertexShaderSource = "#version 330 core\n"
								 "layout (location = 0) in vec3 aPos;\n"
								 "void main()\n"
								 "{\n"
								 "   gl_Position = vec4(aPos.x, aPos.y, aPos.z, 1.0);\n"
								 "}\0";
const char *fragmentShaderSource = "#version 330 core\n"
								   "out vec4 FragColor;\n"
								   "void main()\n"
								   "{\n"
								   "   FragColor = vec4(1.0f, 0.5f, 0.2f, 1.0f);\n"
								   "}\n\0";

//random between 0 and 1
float get_random()
{
	int min = 0;
	int max = 1000;

	int randNum = rand() % (max - min + 1) + min;

	float r = (float)randNum / (float)1000;
	return r;
}

int main()
{
	// glfw: initialize and configure
	// ------------------------------
	int n;
	std::cout << "enter n value:\n";
	std::cin >> n;
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef __APPLE__
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

	// glfw window creation
	// --------------------
	GLFWwindow *window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "LearnOpenGL", NULL, NULL);
	if (window == NULL)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

	// glad: load all OpenGL function pointers
	// ---------------------------------------
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
		return -1;
	}

	// build and compile our shader program
	Shader ourShader("../src/vertex.shader", "../src/fragment.shader");

	// build and compile our shader program
	// ------------------------------------
	// vertex shader
	unsigned int vertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
	glCompileShader(vertexShader);
	// check for shader compile errors
	int success;
	char infoLog[512];
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n"
				  << infoLog << std::endl;
	}
	// fragment shader
	unsigned int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
	glCompileShader(fragmentShader);
	// check for shader compile errors
	glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n"
				  << infoLog << std::endl;
	}
	// link shaders
	unsigned int shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);
	glLinkProgram(shaderProgram);
	// check for linking errors
	glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
	if (!success)
	{
		glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n"
				  << infoLog << std::endl;
	}
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);

	// set up vertex data (and buffer(s)) and configure vertex attributes

	float vertices[n * 3 * 2 * 2];
	int w = 0;
	// float r=0.5;
	float r = 0.3;
	float t = 0;
	float x = 0.0f;

	for (int i = 0; i < n; i++)
	{

		// vertices[w]=0.5*(r * cos(2 * M_PI * i / n));
		vertices[w] = t + (float)(r * cos(2 * M_PI * i / n));

		std::cout << vertices[w] << std::endl;
		// vertices[w+1]=0.5*(r * sin(2 * M_PI * i / n));

		w++;
		vertices[w] = t + (float)(r * sin(2 * M_PI * i / n));

		std::cout << vertices[w] << std::endl;
		w++;
		vertices[w] = 0.0f;
		w++;

		// colors
		// std::cout
		// 	<< "colors:" << std::endl;
		// vertices[w] = get_random();
		// std::cout << vertices[w] << std::endl;
		// w++;
		// vertices[w] = get_random();
		// std::cout << vertices[w] << std::endl;

		// w++;
		// vertices[w] = get_random();
		// w++;
		// std::cout << vertices[w] << std::endl;
		// std::cout << "end of colors:" << std::endl;
	}

	for (int i = 0; i < n; i++)
	{

		// vertices[w]=0.5*(r * cos(2 * M_PI * i / n));
		vertices[w] = t + (float)(r * cos(2 * M_PI * i / n));

		std::cout << vertices[w] << std::endl;
		// vertices[w+1]=0.5*(r * sin(2 * M_PI * i / n));
		w++;

		vertices[w] = t + (float)(r * sin(2 * M_PI * i / n));

		std::cout << vertices[w] << std::endl;
		w++;
		vertices[w] = 0.5f;
		w++;

		// colors
		// std::cout
		// 	<< "colors:" << std::endl;
		// vertices[w] = get_random();
		// std::cout << vertices[w] << std::endl;
		// w++;
		// vertices[w] = get_random();
		// std::cout << vertices[w] << std::endl;

		// w++;
		// vertices[w] = get_random();
		// w++;
		// std::cout << vertices[w] << std::endl;
		// std::cout << "end of colors:" << std::endl;
	}

	int size = sizeof(vertices) / sizeof(float);
	int no_of_triangles = n - 2;

	// unsigned int indices[] = {
	// 	// note that we start from 0!
	// 	0,
	// 	1,
	// 	2, // first Triangle
	// 	0,
	// 	2,
	// 	3,
	// 	0, 3, 4,
	// 	0, 4, 5};
	int faces = n;

	unsigned int indices[no_of_triangles * 3 * 2 + faces * 2 * 3];
	int k = 0;
	int p = 0;

	for (int i = 0; i < no_of_triangles; i++)
	{
		indices[k] = 0;
		std::cout << k << "--";
		k++;
		indices[k] = p + 1;
		std::cout << k << "--";

		k++;
		indices[k] = p + 2;
		std::cout << k << "--";

		k++;
		p++;
		// p += 3;
	}
	p = n;

	for (int i = 0; i < no_of_triangles; i++)
	{
		indices[k] = n;
		std::cout << k << "--";
		k++;
		indices[k] = p + 1;
		std::cout << k << "--";

		k++;
		indices[k] = p + 2;
		std::cout << k << "--";

		k++;
		p++;
		// p += 3;
	}
	int j = n;
	int a = 0, b = n;
	for (int i = 0; i < n; i++)
	{
		//1st triangle for a face
		indices[k] = a;
		k++;
		a++;
		indices[k] = a;
		k++;
		indices[k] = b;
		k++;
		b++;

		//2nd triangle for a face
		indices[k] = a;
		k++;
		indices[k] = b - 1;
		k++;
		indices[k] = b;
		k++;
	}
	// unsigned int indices[] = {
	// 	// note that we start from 0!
	// 	0,
	// 	1,
	// 	2, // first Triangle
	// 	2,
	// 	3,
	// 	0, // second Triangle
	// 	0,
	// 	4,
	// 	3,
	// };

	// static const GLfloat g_color_buffer_data[]={
	// 	0.583f,  0.771f,  0.014f,
	// 0.609f,  0.115f,  0.436f,
	// 0.327f,  0.483f,  0.844f,
	// 0.822f,  0.569f,  0.201f,
	// 0.435f,  0.602f,  0.223f,
	// 0.310f,  0.747f,  0.185f,
	// 0.597f,  0.770f,  0.761f,
	// 0.559f,  0.436f,  0.730f,
	// 0.359f,  0.583f,  0.152f,
	// 0.483f,  0.596f,  0.789f,
	// 0.559f,  0.861f,  0.639f,
	// 0.195f,  0.548f,  0.859f,
	// 0.014f,  0.184f,  0.576f,
	// 0.771f,  0.328f,  0.970f,
	// 0.406f,  0.615f,  0.116f
	// }

	static const GLfloat g_color_buffer_data[] = {0.583f,
												  0.771f,
												  0.014f,
												  0.609f,
												  0.115f,
												  0.436f,
												  0.327f,
												  0.483f,
												  0.844f,
												  0.822f,
												  0.569f,
												  0.201f,
												  0.435f,
												  0.602f,
												  0.223f,
												  0.310f,
												  0.747f,
												  0.185f,
												  0.597f,
												  0.770f,
												  0.761f,
												  0.559f,
												  0.436f,
												  0.730f,
												  0.359f,
												  0.583f,
												  0.152f,
												  0.483f,
												  0.596f,
												  0.789f,
												  0.559f,
												  0.861f,
												  0.639f,
												  0.195f,
												  0.548f,
												  0.859f,
												  0.014f,
												  0.184f,
												  0.576f,
												  0.771f,
												  0.328f,
												  0.970f,
												  0.406f,
												  0.615f,
												  0.116f};

	unsigned int VBO,
		VAO, EBO;
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &EBO);
	// bind the Vertex Array Object first, then bind and set vertex buffer(s), and then configure vertex attributes(s).
	glBindVertexArray(VAO);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void *)0);
	glEnableVertexAttribArray(0);

	// note that this is allowed, the call to glVertexAttribPointer registered VBO as the vertex attribute's bound vertex buffer object so afterwards we can safely unbind
	glBindBuffer(GL_ARRAY_BUFFER, VBO);

	// remember: do NOT unbind the EBO while a VAO is active as the bound element buffer object IS stored in the VAO; keep the EBO bound.
	//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	// You can unbind the VAO afterwards so other VAO calls won't accidentally modify this VAO, but this rarely happens. Modifying other
	// VAOs requires a call to glBindVertexArray anyways so we generally don't unbind VAOs (nor VBOs) when it's not directly necessary.
	glBindVertexArray(0);

	GLuint colorbuffer;
	glGenBuffers(1, &colorbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_color_buffer_data), g_color_buffer_data, GL_STATIC_DRAW);

	// 2nd attribute buffer : colors
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
	glVertexAttribPointer(
		1,		  // attribute. No particular reason for 1, but must match the layout in the shader.
		3,		  // size
		GL_FLOAT, // type
		GL_FALSE, // normalized?
		0,		  // stride
		(void *)0 // array buffer offset
	);

	// uncomment this call to draw in wireframe polygons.
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	//-------------------------------------------------
	// 	GLuint colorbuffer;
	// glGenBuffers(1, &colorbuffer);
	// glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
	// glBufferData(GL_ARRAY_BUFFER, sizeof(g_color_buffer_data), g_color_buffer_data, GL_STATIC_DRAW);
	// //----------------------------------------------------------

	// // 2nd attribute buffer : colors
	// glEnableVertexAttribArray(1);
	// glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
	// glVertexAttribPointer(
	//     1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
	//     3,                                // size
	//     GL_FLOAT,                         // type
	//     GL_FALSE,                         // normalized?
	//     0,                                // stride
	//     (void*)0                          // array buffer offset
	// );
	//----------------------------------------------------------------------
	// render loop
	// -----------
	while (!glfwWindowShouldClose(window))
	{
		// input
		// -----
		processInput(window);

		// render
		// ------
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		// make transformations
		glm::mat4 transform = glm::mat4(1.0f);
		transform = glm::rotate(transform, (2.0f) * (float)glfwGetTime(),
								glm::vec3(0.0f, 1.0f, 0.0f));
		ourShader.use();
		// ourShader.setMat4("transform", transform);
		unsigned int transformLoc =
			glGetUniformLocation(ourShader.ID, "transform");

		glUniformMatrix4fv(transformLoc, 1, GL_FALSE,
						   glm::value_ptr(transform));
		glBindVertexArray(VAO); // seeing as we only have a single VAO there's no need to bind it every time, but we'll do so to keep things a bit more organized
		//glDrawArrays(GL_TRIANGLES, 0, 6);
		glDrawElements(GL_TRIANGLES, no_of_triangles * 3 * 2 + faces * 2 * 3, GL_UNSIGNED_INT, 0);
		// glBindVertexArray(0); // no need to unbind it every time

		// glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
		// -------------------------------------------------------------------------------
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	// optional: de-allocate all resources once they've outlived their purpose:
	// ------------------------------------------------------------------------

	// glfw: terminate, clearing all previously allocated GLFW resources.
	// ------------------------------------------------------------------
	glfwTerminate();
	return 0;
}

// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void processInput(GLFWwindow *window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);
}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow *window, int width, int height)
{
	// make sure the viewport matches the new window dimensions; note that width and
	// height will be significantly larger than specified on retina displays.
	glViewport(0, 0, width, height);
}

// ourShader.setMat4("transform", transform);

// void setMat4(const std::string &name, const glm::mat4 &mat) const
//     {
//         glUniformMatrix4fv(glGetUniformLocation(ID, name.c_str()), 1, GL_FALSE, &mat[0][0]);
//     }
// unsigned int transformLoc =
// 	glGetUniformLocation(ourShader.ID, "transform");

// glUniformMatrix4fv(transformLoc, 1, GL_FALSE,
// 				   glm::value_ptr(transform));